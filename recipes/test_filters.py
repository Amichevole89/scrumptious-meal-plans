from unittest import TestCase
from recipes.models import Ingredient, Recipe
from recipes.templatetags.resizer import resize_to

print("RECIPES_TEST")


class ResizeToTests(TestCase):
    def test_when_ingredients_are_none(self):
        # Arrange
        recipe = Recipe(servings=None)
        ingredient = Ingredient(recipe=recipe, amount=5)
        print(ingredient)
        # Act
        result = resize_to(ingredient, None)
        # Assert

        print(self.assertEqual(5, result))

    def test_no_resize(self):
        # Arrange
        # Act
        with self.assertRaises(AttributeError):
            resize_to(None, 3)
        # Assert
