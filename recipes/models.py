from multiprocessing import get_context
from click import get_current_context
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.conf import settings
from django.contrib.auth import get_user_model


USER_MODEL = settings.AUTH_USER_MODEL
# Create your models here.


class Recipe(models.Model):
    name = models.CharField(max_length=125)
    # author = models.CharField(max_length=100)
    author = models.ForeignKey(
        USER_MODEL, on_delete=models.CASCADE, related_name="recipes", null=True
    )
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    servings = models.PositiveSmallIntegerField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    updated_by = get_context()

    # request.author.recipes

    def __str__(self):
        return self.name + " by " + str(self.author)

    #     return self.name + " by " + self.author.username


class Measure(models.Model):
    # parent = models.ForeignKey(
    #     "self",
    #     blank=True,
    #     null=True,
    #     verbose_name="parent_measure",
    #     related_name="children",
    #     on_delete=models.CASCADE,
    # )
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    # def get_children_recursive(parent_category):
    #     children = (
    #         parent_category.children.all()
    #     )  # This depends on adding related_name to Category
    #     for child in children:
    #         children += get_children_recursive(child)
    #     return children

    def __str__(self):
        return self.name


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.IntegerField(validators=[MaxValueValidator(20)])
    recipe = models.ForeignKey(
        "Recipe",
        related_name="ingredients",
        on_delete=models.CASCADE,
    )
    measure = models.ForeignKey("Measure", on_delete=models.PROTECT)
    food = models.ForeignKey("FoodItem", on_delete=models.PROTECT)

    def __str__(self):
        amount = str(self.amount)
        measure = self.measure.name
        food = self.food.name
        recipe_name = self.recipe.name
        return amount + " " + measure + " of " + food + " for " + recipe_name


class Step(models.Model):
    recipe = models.ForeignKey(
        "Recipe",
        related_name="steps",
        on_delete=models.CASCADE,
    )
    order = models.PositiveSmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField("FoodItem", blank=True)

    def __str__(self):
        return str(self.order) + ". " + self.directions


class Rating(models.Model):
    value = models.PositiveSmallIntegerField(
        validators=[
            MaxValueValidator(5),
            MinValueValidator(1),
        ]
    )
    recipe = models.ForeignKey(
        "Recipe",
        related_name="ratings",
        on_delete=models.CASCADE,
    )


class ShoppingItem(models.Model):
    user = models.ForeignKey(
        USER_MODEL,
        on_delete=models.CASCADE,
        related_name="shopping_list",
        null=True,
    )

    food_item = models.ForeignKey(
        "FoodItem",
        on_delete=models.PROTECT,
        related_name="food_item",
        null=True,
    )

    def __str__(self):
        return str(self.food_item)
