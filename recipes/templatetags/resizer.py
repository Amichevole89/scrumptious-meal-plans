from django import template

register = template.Library()


# def resize_to(ingredient, target):
#     print("value:", ingredient)
#     print("arg:", target)
#     return "resize done"


def resize_to(ingredient, target):
    # sets str(target) to int(target) because target is input
    # sets amount to ingredient.amount
    amount = ingredient.amount
    # sets servings to ingredient.recipe.servings (current set servings amount)
    servings = ingredient.recipe.servings

    # if the serving is not the target,
    # and the target is not 0 (input must be > 0)
    if servings is not target and target is not None:
        try:
            # print(integer(target), servings, amount)
            # this is when the target(desired servings) is less than the
            # servings currently in the recipe
            return (int(target) / servings) * amount
            # if target smaller than serving
        except target < servings:
            # divide servings by integer(target) * amount
            # this is for when the target(desired servings) is more than
            # servings currently in the recipe
            return (servings / int(target)) * amount
    # TEST PRINTS
    # print(servings)
    # print(amount)
    # print(ingredient.amount)
    return amount


register.filter(resize_to)
