from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse_lazy
from django.db import IntegrityError
from django.db.models import Count
from django.contrib.auth import get_user_model

from django.core.paginator import Paginator
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import RecipeForm, MeasureForm
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_http_methods

from recipes.forms import RatingForm

# try:
from recipes.models import Recipe, ShoppingItem, Ingredient, Measure

# except Exception:
#     RecipeForm = None
#     Recipe = None


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
    if form.is_valid():
        try:
            rating = form.save(commit=False)
            rating.recipe = Recipe.objects.get(pk=recipe_id)
            rating.save()
        except Recipe.DoesNotExist:
            return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)


def list_recipes(request):
    return render(
        request, "recipes/list.html", {"recipe_list": Recipe.objects.all()}
    )


# def single_recipe(request, id):
#     return render(
#         request,
#         "recipes/detail.html",
#         {"single_recipe": get_object_or_404(Recipe, id=id)},
#     )
def single_recipe(request, pk):
    recipe = get_object_or_404(Recipe, pk=pk)

    rating_form = RatingForm()
    foods = []
    if request.user.is_authenticated:
        for item in request.user.shopping_list.all():
            foods.append(item.food_item)

    servings = request.GET.get("servings")

    context = {
        "single_recipe": recipe,
        "rating_form": rating_form,
        "servings": servings,
        "food_in_shopping_list": foods,
    }

    return render(request, "recipes/detail.html", context)


# def create_recipe(request):
#     if request.method == "POST":
#         recipe_form = RecipeForm(request.POST)
#         if recipe_form.is_valid():
#             recipe_form.save()
#             return redirect("recipe_list")
#     recipe_form = RecipeForm()

#     return render(
#         request,
#         "recipes/create.html",
#         {"recipe_form": recipe_form},
#     )
@login_required
def create_recipe(request):
    form = RecipeForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        recipe = form.save(commit=False)
        recipe.author = request.user
        recipe.save()
        return redirect("recipes_list")

    context = {
        "form": form,
    }
    return render(request, "recipes/new.html", context)


# def update_recipe(request, pk):
#     if request.method == "POST":
#         recipe_form = RecipeForm(request.POST)

#         if recipe_form.is_valid():
#             recipe_form.save()

#             return reverse_lazy("recipe_list")
#     recipe_form = RecipeForm()
#     return render(
#         request,
#         "recipes/edit.html",
#         {
#             "recipe_form": recipe_form,
#         },
#     )
@login_required
def edit_recipe(request, pk):
    recipe = get_object_or_404(Recipe, pk=pk)
    form = RecipeForm(
        request.POST or None, request.FILES or None, instance=recipe
    )
    if form.is_valid():
        recipe = form.save(commit=False)
        recipe.author = request.user
        recipe.save()
        return redirect("recipes_list")

    context = {
        "form": form,
        "edit_recipe": recipe,
    }
    return render(request, "recipes/edit.html", context)


@login_required
def recipe_delete_view(request, pk):
    recipe = get_object_or_404(Recipe, pk=pk)

    if request.method == "POST":
        recipe.author = request.user
        recipe.delete()
        return redirect("recipes_list")

    context = {"delete_recipe": recipe}
    return render(request, "recipes/delete.html", context)


def user_list_view(request):
    User = get_user_model()

    recipes = Recipe.objects.select_related("author").order_by("-updated_at")
    paginator = Paginator(recipes, 10)
    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)

    users = User.objects.annotate(recipe_count=Count("recipes"))

    context = {
        "users": users,
        "page_obj": page_obj,
        "name_field": "name",
    }

    return render(request, "users/users.html", context)


@login_required
def list_shopping_items(request):
    shopping_list_items = ShoppingItem.objects.filter(user=request.user)
    context = {
        "shopping_list_items": shopping_list_items,
    }
    return render(request, "shopping_list/list.html", context)


@require_http_methods("POST")
def NewShoppingItem(request):
    # gets ingredient id from post
    ingredient_id = request.POST.get("sauce")
    # get specific ingredient from ingredient model
    ingredient = Ingredient.objects.get(id=ingredient_id)
    # set user to existing user id
    user = request.user
    try:
        # create new shopping_item in the database
        ShoppingItem.objects.create(food_item=ingredient.food, user=user)
    except IntegrityError:
        # do nothing with the error
        pass
    # go back to the recipe page
    return redirect("recipe_detail", pk=ingredient.recipe.id)


# function view to delete all shopping items
def ClearShoppingList(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_list")


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 5


class UserListView(ListView):
    model = Recipe
    template_name = "users/users.html"
    success_url = reverse_lazy("users_list")
    paginate_by = 10
    fields = ["name"]

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["user"] = Recipe.updated_by
        return context

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        foods = []
        for item in self.request.user.shopping_list.all():
            foods.append(item.food_item)
        context["servings"] = self.request.GET.get("servings")
        context["food_in_shopping_list"] = foods
        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"

    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeDeleteView(DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shopping_list/list.html"
    success_url = reverse_lazy("shopping_list")
    context_object_name = "shopping_list_items"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


# @require_http_methods("POST")
# def NewShoppingItem(request):
#     # gets ingredient id from post
#     ingredient_id = request.POST.get("sauce")
#     # get specific ingredient from ingredient model
#     ingredient = Ingredient.objects.get(id=ingredient_id)
#     # set user to existing user id
#     user = request.user
#     try:
#         # create new shopping_item in the database
#         ShoppingItem.objects.create(food_item=ingredient.food, user=user)
#     except IntegrityError:
#         # do nothing with the error
#         pass
#     # go back to the recipe page
#     return redirect("recipe_detail", pk=ingredient.recipe.id)


# # function view to delete all shopping items
# def ClearShoppingList(request):
#     ShoppingItem.objects.filter(user=request.user).delete()
#     return redirect("shopping_list")
