from django.forms import ModelForm
from recipes.models import Measure, Rating, Recipe, Ingredient, FoodItem, Step


class RatingForm(ModelForm):
    class Meta:
        model = Rating
        fields = ["value"]


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "name",
            "image",
            "description",
            "image",
            "servings",
        ]


class MeasureForm(ModelForm):
    class Meta:
        model = Measure
        fields = ["name", "abbreviation"]


class FoodItemForm(ModelForm):
    class Meta:
        model = FoodItem
        fields = ["name"]


class IngredientForm(ModelForm):
    class Meta:
        model = Ingredient
        fields = ["amount", "recipe", "measure", "food"]


class StepForm(ModelForm):
    class Meta:
        model = Step
        fields = ["recipe", "order", "directions", "food_items"]
