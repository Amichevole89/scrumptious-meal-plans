from django.urls import path

from meal_plans.views import (
    MealplanCreateView,
    MealplanDeleteView,
    MealplanUpdateView,
    MealplanDetailView,
    MealplanListView,
)

urlpatterns = [
    path("", MealplanListView.as_view(), name="meal_plans_list"),
    path("<int:pk>/", MealplanDetailView.as_view(), name="meal_plans_detail"),
    path("new/", MealplanCreateView.as_view(), name="meal_plans_new"),
    path(
        "<int:pk>/edit/", MealplanUpdateView.as_view(), name="meal_plans_edit"
    ),
    path(
        "<int:pk>/delete/",
        MealplanDeleteView.as_view(),
        name="meal_plans_delete",
    ),
]
