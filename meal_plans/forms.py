from django.forms import ModelForm, DateInput, DateField

from meal_plans.models import Mealplan


class CustomDateInput(DateInput):
    input_type = "date"


class MealPlanForm(ModelForm):
    date = DateField(widget=CustomDateInput)

    class Meta:
        model = Mealplan
        fields = ["name", "recipes", "date"]
