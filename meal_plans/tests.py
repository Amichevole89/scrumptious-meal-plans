# from django.test import TestCase
# from meal_plans.models import Mealplan
# from django.utils import timezone
# from django.db.utils import IntegrityError
# from django.contrib.auth.models import User

# # Create your tests here.

# print("MEALPLAN_TEST")


# class MealplanTestCase(TestCase):
#     def test_meal_plans_must_have_a_user(self):
#         # Arrange
#         with self.assertRaises(IntegrityError):
#             Mealplan.objects.create(
#                 name="This weeks", date=timezone.now(), owner=None
#             )
#         # Act

#         # Assert

#     def test_meal_plans_need_to_set_the_name_properly(self):
#         # Arrange
#         name = "My Meal Plan"
#         owner = User.objects.create(username="test_user")
#         # Act
#         meal_plan = Mealplan.objects.create(
#             name=name, date=timezone.now(), owner=owner
#         )
#         # Assert
#         self.assertEqual(meal_plan.name, name)
