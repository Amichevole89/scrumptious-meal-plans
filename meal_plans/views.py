from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView
from django.views.generic import CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin

from meal_plans.models import Mealplan
from meal_plans.forms import MealPlanForm


class MealplanListView(LoginRequiredMixin, ListView):
    model = Mealplan
    template_name = "meal_plans/list.html"
    context_object_name = "meal_plans_list"
    success_url = reverse_lazy("meal_plans_list")

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)


class MealplanDetailView(LoginRequiredMixin, DetailView):
    model = Mealplan
    template_name = "meal_plans/detail.html"
    context_object_name = "meal_plans_detail"

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)


class MealplanCreateView(LoginRequiredMixin, CreateView):
    model = Mealplan
    template_name = "meal_plans/new.html"
    # fields = "name", "date", "owner", "recipes"
    success_url = reverse_lazy("meal_plans_list")
    context_object_name = "meal_list"
    form_class = MealPlanForm

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plans_detail", pk=plan.id)


class MealplanUpdateView(LoginRequiredMixin, UpdateView):
    model = Mealplan
    template_name = "meal_plans/edit.html"
    fields = "name", "date", "recipes"
    success_url = reverse_lazy("meal_plans_list")

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plans_detail", pk=plan.id)


class MealplanDeleteView(LoginRequiredMixin, DeleteView):
    model = Mealplan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")
    context_object_name = "meal_plans_delete"

    def get_queryset(self):
        return Mealplan.objects.filter(owner=self.request.user)
